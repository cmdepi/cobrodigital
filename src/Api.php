<?php
/**
 *
 * @description CobroDigital API v3.0
 *
 * @author de Picciotto, C. M. <https://www.cmdepi.com> <cmdepicciotto@gmail.com>
 *
 */
namespace Cmdepi\CobroDigital;
class Api
{
    /**
     *
     * API Get Payer attribute
     *
     * @const GET_PAYER_STRUCTURE_RESOURCE
     *
     */
    const GET_PAYER_STRUCTURE_RESOURCE = 'consultar_estructura_pagadores';

    /**
     *
     * API Create Payer Resource
     *
     * @const CREATE_PAYER_RESOURCE
     *
     */
    const CREATE_PAYER_RESOURCE = 'crear_pagador';

    /**
     *
     * API Edit Payer Resource
     *
     * @const EDIT_PAYER_RESOURCE
     *
     */
    const EDIT_PAYER_RESOURCE = 'editar_pagador';

    /**
     *
     * API Payer Existence Validator Resource
     *
     * @const PAYERS_EXISTENCE_VALIDATOR_RESOURCE
     *
     */
    const PAYERS_EXISTENCE_VALIDATOR_RESOURCE = 'verificar_existencia_pagador';

    /**
     *
     * API Create Payment Ticket Resource
     *
     * @const CREATE_PAYMENT_TICKET_RESOURCE
     *
     */
    const CREATE_PAYMENT_TICKET_RESOURCE = 'generar_boleta';

    /**
     *
     * API Disable Payment Ticket Resource
     *
     * @const DISABLE_PAYMENT_TICKET_RESOURCE
     *
     */
    const DISABLE_PAYMENT_TICKET_RESOURCE = 'inhabilitar_boleta';

    /**
     *
     * API Get Payment Ticket HTML Resource
     *
     * @const GET_PATMENT_TICKET_HTML_RESOURCE
     *
     */
    const GET_PAYMENT_TICKET_HTML_RESOURCE = 'obtener_boleta_html';

    /**
     *
     * API Bank Transfer Resource
     *
     * @const BANK_TRANSFER_RESOURCE
     *
     */
    const BANK_TRANSFER_RESOURCE = 'generar_debito_automatico';

    /**
     *
     * API Get Transactions
     *
     * @const GET_TRANSACTIONS_RESOURCE
     *
     */
    const GET_TRANSACTIONS_RESOURCE = 'consultar_transacciones';

    /**
     *
     * API URL
     *
     * @var string $_url
     *
     */
    protected $_url;

    /**
     *
     * API site ID
     *
     * @var string $_idComercio
     *
     */
    protected $_idComercio;

    /**
     *
     * API secret key
     *
     * @var string $_sid
     *
     */
    protected $_sid;

    /**
     *
     * API constructor.
     *
     * @param string $url API URL
     * @param string $idComercio API site ID
     * @param string $sid API secret key
     *
     */
    public function __construct($url, $idComercio, $sid)
    {
        $this->_url        = $url;
        $this->_idComercio = $idComercio;
        $this->_sid        = $sid;
    }

    /**
     *
     * Get payer structure (payer attributes)
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : array with payer attributes
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function getPayerStructure()
    {
        return $this->_doRequest(self::GET_PAYER_STRUCTURE_RESOURCE, \Zend\Http\Request::METHOD_GET);
    }

    /**
     *
     * Create payer
     *
     * @param array $payerData Payer attributes data. Example: array('attribute1' => 'value1', 'attribute2' => 'value2'...)
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : There is not return value to this key
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function createPayer($payerData)
    {
        $data            = array();
        $data['pagador'] = $payerData;
        return $this->_doRequest(self::CREATE_PAYER_RESOURCE, \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Edit payer
     *
     * @param string $identifierAttribute Payer identifier attribute name
     * @param string|int $identifierValue Payer identifier attribute value
     * @param array $payerData Payer attributes data to update. Example: array('attribute1' => 'value1', 'attribute2' => 'value2'...)
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : There is not return value to this key
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function editPayer($identifierAttribute, $identifierValue, $payerData)
    {
        $data                  = array();
        $data['identificador'] = $identifierAttribute;
        $data['buscar']        = $identifierValue;
        $data['pagador']       = $payerData;
        return $this->_doRequest(self::EDIT_PAYER_RESOURCE, \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Validate payer existence
     *
     * @param string $identifierAttribute Payer identifier attribute name
     * @param string|int $identifierValue Payer identifier attribute value
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : If 'ejecucion_correcta' == 1 return array(..., 'datos' => array('1')) else this key does not exist
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function payerExists($identifierAttribute, $identifierValue)
    {
        $data                  = array();
        $data['identificador'] = $identifierAttribute;
        $data['buscar']        = $identifierValue;
        return $this->_doRequest(self::PAYERS_EXISTENCE_VALIDATOR_RESOURCE, \Zend\Http\Request::METHOD_GET, $data);
    }

    /**
     *
     * Create payment ticket
     *
     * @param string $identifierAttribute Payer identifier attribute name
     * @param string|int $identifierValue Payer identifier attribute value
     * @param string $concept Payment ticket reason
     * @param string $template Payment ticket template
     * @param array $dueDates Due dates. Date has to have the format 'YYYYMMDD'
     * @param array $amounts Amounts array
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : Return the payment ticket ID. Example: array(..., 'datos' => array('2'))
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function createPaymentTicket($identifierAttribute, $identifierValue, $concept, $template = 'init', $dueDates, $amounts)
    {
        $data                       = array();
        $data['identificador']      = $identifierAttribute;
        $data['buscar']             = $identifierValue;
        $data['concepto']           = $concept;
        $data['plantilla']          = $template;
        $data['fechas_vencimiento'] = $dueDates;
        $data['importes']           = $amounts;
        return $this->_doRequest(self::CREATE_PAYMENT_TICKET_RESOURCE, \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Disable payment ticket
     *
     * @param string $paymentTicketId Payment Ticket ID
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : There is not return value to this key
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function disablePaymentTicket($paymentTicketId)
    {
        $data               = array();
        $data['nro_boleta'] = $paymentTicketId;
        return $this->_doRequest(self::DISABLE_PAYMENT_TICKET_RESOURCE, \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Get payment ticket HTML code
     *
     * @param string $paymentTicketId Payment Ticket ID
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : Array with the Payment Ticket HTML code in json format. Example: array(..., 'datos' => "{'boleta':'HTML'}")
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function getPaymentTicketHTML($paymentTicketId)
    {
        $data               = array();
        $data['nro_boleta'] = $paymentTicketId;
        return $this->_doRequest(self::GET_PAYMENT_TICKET_HTML_RESOURCE, \Zend\Http\Request::METHOD_GET, $data);
    }

    /**
     *
     * Bank Transfer Method
     *
     * @param string $firstname Payer firstname
     * @param string $lastname Payer lastname
     * @param string $cuit Payer document (Argentinian DNI, CUIL or CUIT)
     * @param string $email Payer email
     * @param string $cbu Argentinian payer CBU
     * @param int $amount Amount
     * @param string $date Date with format 'YYYYMMDD'
     * @param string $concept Reason
     * @param int $paymentInstallments Payment installments
     * @param string $paymentInstallmentsMethod Payment installments method
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function bankTransfer($firstname, $lastname, $cuit, $email, $cbu, $amount, $date, $concept, $paymentInstallments = null, $paymentInstallmentsMethod = null)
    {
        $data             = array();
        $data['nombre']   = $firstname;
        $data['apellido'] = $lastname;
        $data['cuit']     = $cuit;
        $data['email']    = $email;
        $data['cbu']      = $cbu;
        $data['importe']  = $amount;
        $data['fecha']    = $date;
        $data['concepto'] = $concept;

        if (!is_null($paymentInstallments)) {
            $data['cuotas'] = $paymentInstallments;
        }

        if (!is_null($paymentInstallmentsMethod)) {
            $data['modalidad_cuotas'] = $paymentInstallmentsMethod;
        }

        return $this->_doRequest(self::BANK_TRANSFER_RESOURCE, \Zend\Http\Request::METHOD_POST, $data);
    }

    /**
     *
     * Get Transactions
     *
     * @param string $from Date from. Date with the form YYYYMMDD
     * @param string $to Date to. Date with the form YYYYMMDD
     * @param array $filters Array to filter result. Array with the form ('nro_boleta' => 'valor1', 'concepto' => 'valor2', 'identificador' => 'valor3', 'nombre' => 'valor4')
     * @param int $offset Offset pagination
     * @param int $limit Limit pagination
     *
     * @return array An array with this data:
     *                  - 'ejecucion_correcta': Boolean to identify if the method runs ok (1) or not (0)
     *                  - 'log'               : Log data
     *                  - 'datos'             : Array with the results. An item has the form array('id_transaccion' => 'value', 'Fecha' => 'value', 'Nro Boleta' => 'value', 'Identificación' => 'value', 'Nombre' => 'value', 'Info' => 'value', 'Concepto' => 'value', 'Importe Bruto' => 'value', 'Comisión' => 'value', 'Importe neto' => 'value', 'Saldo acumulado' => 'value')
     *
     * @note See CobroDigital WebService 3.0 Documentation
     *
     */
    public function getTransactions($from, $to, $filters = null, $offset = null, $limit = null)
    {
        $data          = array();
        $data['desde'] = $from;
        $data['hasta'] = $to;

        if (!is_null($filters)) {
            $data['filtros'] = $filters;
        }

        if (!is_null($offset)) {
            $data['offset'] = $offset;
        }

        if (!is_null($limit)) {
            $data['limit'] = $limit;
        }

        return $this->_doRequest(self::GET_TRANSACTIONS_RESOURCE, \Zend\Http\Request::METHOD_GET, $data);
    }

    /**
     *
     * Do HTTP Request
     *
     * @param string $resource API Resource
     * @param string $method HTTP method
     * @param array  $data Data to send
     *
     * @return array The JSON response as associative array
     *
     */
    protected function _doRequest($resource, $method, $data = array())
    {
        $client = new \Zend\Http\Client();

        /**
         *
         * @note Set API URL
         *
         */
        $client->setUri($this->_url);

        /**
         *
         * @note Set method
         *
         */
        $client->setMethod($method);

        /**
         *
         * @note Set authentication data
         *
         */
        $data['idComercio'] = $this->_idComercio;
        $data['sid']        = $this->_sid;

        /**
         *
         * @note Set resource
         *
         */
        $data['metodo_webservice'] = $resource;

        if($method  == \Zend\Http\Request::METHOD_GET) {
            $client->setParameterGet($data);
        }

        if($method  == \Zend\Http\Request::METHOD_POST) {
            $client->setParameterPost($data);
        }

        $response = $client->send();

        return json_decode($response->getBody(), true);
    }
}
